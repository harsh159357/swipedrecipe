package com.harsh.swipedrecipe.di

import android.app.Application
import com.harsh.swipedrecipe.data.local.AppDatabase
import com.harsh.swipedrecipe.data.remote.RecipeService
import com.harsh.swipedrecipe.data.remote.datasource.RecipeDataSource
import com.harsh.swipedrecipe.util.Constants
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import kotlin.coroutines.CoroutineContext

@Module(includes = [ViewModelModule::class, CoreDataModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideFactsService(@NewAPI okHttpClient: OkHttpClient,
                            converterFactory: GsonConverterFactory
    ) = provideService(okHttpClient, converterFactory, RecipeService::class.java)

    @Singleton
    @Provides
    fun provideRemoteDataSource(recipeService: RecipeService)
            = RecipeDataSource(recipeService)

    @Provides
    @Singleton
    fun provideCoroutineContext(): CoroutineContext {
        return Dispatchers.Main
    }

    @CoroutineScopeIO
    @Provides
    fun provideCoroutineScopeIO() = CoroutineScope(Dispatchers.IO)

    @NewAPI
    @Provides
    fun providePrivateOkHttpClient(
        upstreamClient: OkHttpClient
    ): OkHttpClient {
        return upstreamClient.newBuilder().build()
    }

    @Singleton
    @Provides
    fun provideDb(app: Application) = AppDatabase.getInstance(app)

    @Singleton
    @Provides
    fun provideFactsDao(db: AppDatabase) = db.factsDao()

    private fun createRetrofit(
        okhttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .client(okhttpClient)
            .addConverterFactory(converterFactory)
            .build()
    }

    private fun <T> provideService(okhttpClient: OkHttpClient,
                                   converterFactory: GsonConverterFactory, factsClass: Class<T>): T {
        return createRetrofit(okhttpClient, converterFactory).create(factsClass)
    }
}