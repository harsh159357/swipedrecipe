package com.harsh.swipedrecipe.di

import androidx.lifecycle.ViewModel
import com.harsh.swipedrecipe.ui.recipes.RecipesListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(RecipesListViewModel::class)
    abstract fun factsListViewModel(viewModel: RecipesListViewModel): ViewModel


}