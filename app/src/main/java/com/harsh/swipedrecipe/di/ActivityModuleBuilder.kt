package com.harsh.swipedrecipe.di

import com.harsh.swipedrecipe.ui.cart.CartActivity
import com.harsh.swipedrecipe.ui.recipes.RecipesActivity
import com.harsh.swipedrecipe.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityModuleBuilder {

    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): RecipesActivity

    @ContributesAndroidInjector
    abstract fun contributeCartActivity(): CartActivity

}