package com.harsh.swipedrecipe.util

import com.harsh.swipedrecipe.R

class Constants {

    companion object {
        const val SPLASH_DELAY = 2000

        const val BASE_URL = "https://s3-ap-southeast-1.amazonaws.com/"

        const val RECIPES_JSON = "he-public-data/reciped9d7b8c.json"

        const val RECIPES_END_POINT: String = BASE_URL + RECIPES_JSON


        //Some sample splash animations
        val splashAnimation = intArrayOf(
            R.anim.fade_in,
            R.anim.zoom_in,
            R.anim.slide_down,
            R.anim.bounce_down,
            R.anim.rotate_clockwise,
            R.anim.rotate_anti_clockwise
        )

    }
}