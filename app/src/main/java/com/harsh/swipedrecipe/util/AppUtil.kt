package com.harsh.swipedrecipe.util

import android.content.Context
import android.view.View
import android.widget.Toast
import java.security.SecureRandom
import java.util.*

fun getRandomSplashAnimation(): Int {
    return Constants.splashAnimation.get(
        generateRandomInteger(
            0,
            Constants.splashAnimation.size - 1
        )
    )
}

private fun generateRandomInteger(min: Int, max: Int): Int {
    val rand = SecureRandom()
    rand.setSeed(Date().time)
    return rand.nextInt(max - min + 1) + min
}


fun Context.toast(message: CharSequence) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun View.showView() {
    this.visibility = View.VISIBLE
}

fun View.hideView() {
    this.visibility = View.GONE
}