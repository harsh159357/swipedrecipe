package com.harsh.swipedrecipe.util

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory
import com.harsh.swipedrecipe.R


fun bindImageFromUrl(imageView: ImageView, imageUrl: String?) {

    imageUrl?.let {

//        val image = it.replace("http", "https")
//
//        val requestOptions = RequestOptions()
//        val width: Int = imageView.context.resources.getDimensionPixelOffset(R.dimen.dp200)
//        val height: Int = imageView.context.resources.getDimensionPixelOffset(R.dimen.dp80)
//
//        requestOptions.override(width, height)
//
        val factory =
            DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build()

        Glide.with(imageView.context)
            .load(it)
            .transition(withCrossFade(factory))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(R.drawable.ic_placeholder)
//            .apply(requestOptions)
            .into(imageView)
    }
}