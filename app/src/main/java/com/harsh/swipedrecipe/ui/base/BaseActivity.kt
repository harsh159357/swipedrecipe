package com.harsh.swipedrecipe.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.harsh.swipedrecipe.ui.base.listeners.BaseView
import dagger.android.AndroidInjection

abstract class BaseActivity : AppCompatActivity(), BaseView {

    abstract val layoutId: Int

    protected abstract fun initializeViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        initializeViewModel()
    }
}