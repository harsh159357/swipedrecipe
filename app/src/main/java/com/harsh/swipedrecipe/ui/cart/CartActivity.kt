package com.harsh.swipedrecipe.ui.cart

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.harsh.swipedrecipe.R
import com.harsh.swipedrecipe.data.local.model.Recipe
import com.harsh.swipedrecipe.ui.base.BaseActivity
import com.harsh.swipedrecipe.ui.cart.adapter.CartAdapter
import com.harsh.swipedrecipe.util.hideView
import com.harsh.swipedrecipe.util.showView
import kotlinx.android.synthetic.main.activity_cart.*
import java.util.*

class CartActivity : BaseActivity() {

    override val layoutId: Int
        get() = R.layout.activity_cart

    companion object {
        @JvmStatic
        fun createIntent(context: Context): Intent = Intent(context, CartActivity::class.java)
    }

    override fun initializeViewModel() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        var cartMap: HashMap<Long, ArrayList<Recipe>> =
            intent.getSerializableExtra(getString(R.string.cart)) as HashMap<Long, ArrayList<Recipe>>
        supportActionBar?.title = getString(R.string.cart)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        if (cartMap.isNullOrEmpty()) {
            empty_container.showView()
            rv_cart.hideView()
        } else {
            val cartAdapter = CartAdapter()
            cartAdapter.setCartMap(cartMap)
            rv_cart.adapter = cartAdapter
            cartAdapter.notifyDataSetChanged()
        }
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(menuItem)
    }
}
