package com.harsh.swipedrecipe.ui.cart.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.harsh.swipedrecipe.R
import com.harsh.swipedrecipe.data.local.model.Recipe
import java.util.*
import kotlin.collections.ArrayList

class CartAdapter(
    private var cartMap: HashMap<Long, ArrayList<Recipe>> = HashMap<Long, ArrayList<Recipe>>()
) : RecyclerView.Adapter<CartAdapter.ViewHolder>() {

    var index: ArrayList<Long> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_cart, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val recipe = cartMap[index.get(position)]?.get(0)
        holder.name.text = recipe?.name
        holder.descrption.text = recipe?.description
        holder.extra.text = "Price: ${recipe?.price} & Qty: ${cartMap[index.get(position)]?.size}"
        Glide.with(holder.recipeImage)
            .load(recipe?.image)
            .placeholder(R.drawable.ic_placeholder)
            .into(holder.recipeImage)
        holder.itemView.setOnClickListener { v ->
            Toast.makeText(v.context, recipe?.name, Toast.LENGTH_SHORT).show()
        }
    }


    fun setCartMap(cartMap: HashMap<Long, ArrayList<Recipe>>) {

        for ((key, value) in cartMap) {
            index.add(key)
        }

        this.cartMap = cartMap
    }

    fun getCartMap(): HashMap<Long, ArrayList<Recipe>> {
        return cartMap
    }

    override fun getItemCount(): Int {
        return cartMap.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.txtRecipeName)
        var descrption: TextView = view.findViewById(R.id.txtRecipeDesc)
        var recipeImage: ImageView = view.findViewById(R.id.imgRecipe)
        var extra: TextView = view.findViewById(R.id.txtExtra)
    }

}
