package com.harsh.swipedrecipe.ui.recipes

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.TextView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.harsh.swipedrecipe.R
import com.harsh.swipedrecipe.data.local.model.Recipe
import com.harsh.swipedrecipe.data.remote.Result
import com.harsh.swipedrecipe.di.ViewModelFactory
import com.harsh.swipedrecipe.di.injectViewModel
import com.harsh.swipedrecipe.ui.base.BaseActivity
import com.harsh.swipedrecipe.ui.cart.CartActivity
import com.harsh.swipedrecipe.ui.recipes.adapter.RecipesAdapter
import com.harsh.swipedrecipe.util.*
import com.yuyakaido.android.cardstackview.*
import kotlinx.android.synthetic.main.activity_recipes.*
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


class RecipesActivity : BaseActivity(),
    SwipeRefreshLayout.OnRefreshListener, NetworkStateReceiver.NetworkStateReceiverListener,
    CardStackListener {

    private var netWorkStateReceiver: NetworkStateReceiver? = null

    companion object {
        @JvmStatic
        fun createIntent(context: Context): Intent = Intent(context, RecipesActivity::class.java)
    }

    override val layoutId: Int
        get() = R.layout.activity_recipes

    @Inject
    lateinit var viewModel: ViewModelFactory

    private lateinit var recipesListViewModel: RecipesListViewModel

    private var recipesAdapter: RecipesAdapter? = null
    private var cardStackLayoutManager: CardStackLayoutManager? = null
    private var cartMap: HashMap<Long, ArrayList<Recipe>> = HashMap()
    private var recipes: ArrayList<Recipe> = ArrayList()
    private var disappearPositionId = 0


    override fun initializeViewModel() {
        recipesListViewModel = injectViewModel(viewModel)
        recipesListViewModel.connectivityAvailable = ConnectivityUtil.isConnected(this)
    }

    override fun onResume() {
        super.onResume()
        initViews()
    }

    private fun initViews() {
        initSwipeToRefresh()

        initCards()

        initButtons()

        netWorkStateReceiver = NetworkStateReceiver(this)
        netWorkStateReceiver!!.addListener(this)
        registerReceiver(
            netWorkStateReceiver,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(netWorkStateReceiver)
    }

    private fun initSwipeToRefresh() {
        swipeRefreshLayout.setOnRefreshListener(this)
        swipeRefreshLayout.setColorSchemeResources(
            R.color.colorPrimary,
            android.R.color.holo_green_dark,
            android.R.color.holo_orange_dark
        )
    }

    private fun initButtons() {
        skip_button.setOnClickListener {
            val setting = SwipeAnimationSetting.Builder()
                .setDirection(Direction.Left)
                .setDuration(Duration.Normal.duration)
                .setInterpolator(AccelerateInterpolator())
                .build()
            cardStackLayoutManager?.setSwipeAnimationSetting(setting)
            card_stack_view.swipe()
        }

        reload_button.setOnClickListener {
            val setting = RewindAnimationSetting.Builder()
                .setDirection(Direction.Bottom)
                .setDuration(Duration.Normal.duration)
                .setInterpolator(DecelerateInterpolator())
                .build()
            cardStackLayoutManager?.setRewindAnimationSetting(setting)
            card_stack_view.rewind()
        }

        like_button.setOnClickListener {
            val setting = SwipeAnimationSetting.Builder()
                .setDirection(Direction.Right)
                .setDuration(Duration.Normal.duration)
                .setInterpolator(AccelerateInterpolator())
                .build()
            cardStackLayoutManager?.setSwipeAnimationSetting(setting)
            card_stack_view.swipe()
        }

        cart_button.setOnClickListener {
            val intent = CartActivity.createIntent(this)
            intent.putExtra(getString(R.string.cart), cartMap)
            startActivity(intent)
        }
    }

    private fun initCards() {
        recipesAdapter = RecipesAdapter()
        cardStackLayoutManager = CardStackLayoutManager(this, this)
        card_stack_view.layoutManager = cardStackLayoutManager
        card_stack_view.adapter = recipesAdapter
        recipesAdapter?.let {
            subscribeUiForCards(it)
        }
        recipesListViewModel.getRecipes()
    }

    private fun subscribeUiForCards(adapter: RecipesAdapter) {
        recipesListViewModel.mutableListLiveDataResult.observe(this, { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    card_stack_view.showView()
                    button_container.showView()
                    result.title?.let {
                        supportActionBar?.setDisplayShowTitleEnabled(true)
                        if (it.isNotEmpty()) {
                            supportActionBar?.title = it
                        } else {
                            supportActionBar?.title = getString(R.string.recipes)
                        }
                    }
                    result.data?.let {
                        recipes = it as ArrayList<Recipe>
                        adapter.setRecipes(it)
                        if (it.isNotEmpty()) {
                            empty_container.hideView()
                            card_stack_view.showView()
                            button_container.showView()
                        } else {
                            empty_container.showView()
                            card_stack_view.hideView()
                            button_container.hideView()
                        }
                    }
                    swipeRefreshLayout.isRefreshing = false
                }
                Result.Status.LOADING -> {
                    card_stack_view.hideView()
                    swipeRefreshLayout.isRefreshing = true
                }
                Result.Status.ERROR -> {
                    swipeRefreshLayout.isRefreshing = false
                    result.message?.let {
                        empty_container.showView()
                        card_stack_view.hideView()
                        button_container.hideView()
                        Snackbar.make(card_stack_view, it, Snackbar.LENGTH_LONG).show()
                    }
                }
            }
        })
    }

    override fun onRefresh() {
        recipesListViewModel.getRecipes()
        recipesAdapter?.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_recipes, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_refresh -> onRefresh()
            R.id.menu_price_ascending -> {
                Collections.sort(recipes, PriceAscending())
                recipesAdapter?.setRecipes(recipes)
                recipesAdapter?.notifyDataSetChanged()
            }
            R.id.menu_price_descending -> {
                Collections.sort(recipes, PriceDescending())
                recipesAdapter?.setRecipes(recipes)
                recipesAdapter?.notifyDataSetChanged()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setMessage(R.string.exit)
            .setPositiveButton(
                R.string.yes
            ) { dialog, which -> finish() }
            .setNegativeButton(
                R.string.no
            ) { dialog, which -> dialog.cancel() }
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }

    override fun onNetworkAvailable() {
        if (empty_container.visibility == View.VISIBLE) {
            onRefresh()
        }
    }

    override fun onNetworkUnavailable() {
    }

    override fun onCardDragging(direction: Direction, ratio: Float) {
        Log.d("CardStackView", "onCardDragging: d = ${direction.name}, r = $ratio")
    }

    override fun onCardSwiped(direction: Direction) {
        val position = cardStackLayoutManager?.topPosition
        Log.d(
            "CardStackView",
            "onCardSwiped: p = ${position}, d = $direction"
        )


        if (direction == Direction.Left) {
            toast("Recipe Skipped")
        } else {
            val recipe = recipesAdapter?.getRecipes()!![disappearPositionId]
            var list = cartMap[recipe.id]
            if (list != null) {
                list.add(recipe)
                cartMap[recipe.id] = list
            } else {
                list = ArrayList()
                list.add(recipe)
                cartMap[recipe.id] = list
            }
            toast("Recipe Added to Cart")
        }
    }

    override fun onCardRewound() {
        Log.d("CardStackView", "onCardRewound: ${cardStackLayoutManager?.topPosition}")
        toast("Recipe Rewind")
    }

    override fun onCardCanceled() {
        Log.d("CardStackView", "onCardCanceled: ${cardStackLayoutManager?.topPosition}")
    }

    override fun onCardAppeared(view: View, position: Int) {
        val textView = view.findViewById<TextView>(R.id.item_recipe_name)
        Log.d("CardStackView", "onCardAppeared: ($position) ${textView.text}")
    }

    override fun onCardDisappeared(view: View, position: Int) {
        val textView = view.findViewById<TextView>(R.id.item_recipe_id)
        disappearPositionId = textView.text.toString().toInt()
        Log.d("CardStackView", "onCardDisappeared: ($position) ${textView.text}")
    }

    internal class PriceAscending : Comparator<Recipe> {
        override fun compare(recipe1: Recipe, recipe2: Recipe): Int {
            return if (recipe1.price == recipe2.price) 0 else if (recipe1.price > recipe2.price) 1 else -1
        }
    }

    internal class PriceDescending : Comparator<Recipe> {
        override fun compare(recipe1: Recipe, recipe2: Recipe): Int {
            return if (recipe1.price == recipe2.price) 0 else if (recipe1.price < recipe2.price) 1 else -1
        }
    }

}
