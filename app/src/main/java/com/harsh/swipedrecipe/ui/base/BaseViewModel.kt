package com.harsh.swipedrecipe.ui.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel: ViewModel()