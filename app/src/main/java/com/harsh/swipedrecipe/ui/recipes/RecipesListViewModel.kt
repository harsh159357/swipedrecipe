package com.harsh.swipedrecipe.ui.recipes

import androidx.lifecycle.MutableLiveData
import com.harsh.swipedrecipe.data.RecipeRepository
import com.harsh.swipedrecipe.data.local.model.Recipe
import com.harsh.swipedrecipe.data.remote.Result
import com.harsh.swipedrecipe.di.CoroutineScopeIO
import com.harsh.swipedrecipe.ui.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import javax.inject.Inject

class RecipesListViewModel @Inject constructor(
    private val recipeRepository: RecipeRepository,
    @CoroutineScopeIO private val coroutineScope: CoroutineScope
) : BaseViewModel() {

    var mutableListLiveDataResult: MutableLiveData<Result<List<Recipe>>> = MutableLiveData()
    var connectivityAvailable = false

    /**
     * Get Recipes list from data repository
     */
    fun getRecipes() {
        recipeRepository.observeRecipes(coroutineScope) {
            mutableListLiveDataResult.postValue(it)
        }
    }

    /**
     * Cancel all coroutines when the ViewModel is cleared.
     */
    override fun onCleared() {
        super.onCleared()
        coroutineScope.cancel()
    }

}