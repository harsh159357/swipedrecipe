package com.harsh.swipedrecipe.ui.recipes.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.harsh.swipedrecipe.R
import com.harsh.swipedrecipe.data.local.model.Recipe

class RecipesAdapter(
    private var recipes: List<Recipe> = emptyList()
) : RecyclerView.Adapter<RecipesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_recipe_card, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val recipe = recipes[position]
        holder.id.text = "${recipe.id}"
        holder.price.text = "${recipe.price}"
        holder.name.text = recipe.name
        holder.descrption.text = recipe.description
        Glide.with(holder.recipeImage)
            .load(recipe.image)
            .placeholder(R.drawable.ic_placeholder)
            .into(holder.recipeImage)
        holder.itemView.setOnClickListener { v ->
            Toast.makeText(v.context, recipe.name, Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int {
        return recipes.size
    }

    fun setRecipes(spots: List<Recipe>) {
        this.recipes = spots
    }

    fun getRecipes(): List<Recipe> {
        return recipes
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var id: TextView = view.findViewById(R.id.item_recipe_id)
        var price: TextView = view.findViewById(R.id.item_recipe_price)
        val name: TextView = view.findViewById(R.id.item_recipe_name)
        var descrption: TextView = view.findViewById(R.id.item_description)
        var recipeImage: ImageView = view.findViewById(R.id.item_recipe_image)
    }

}
