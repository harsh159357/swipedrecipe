package com.harsh.swipedrecipe.data

import android.util.Log
import com.harsh.swipedrecipe.data.local.LocalRepository
import com.harsh.swipedrecipe.data.local.model.Recipe
import com.harsh.swipedrecipe.data.remote.Result
import com.harsh.swipedrecipe.data.remote.datasource.RecipeDataSource
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RecipeRepository @Inject
constructor(
    private val recipeDataSource: RecipeDataSource,
    private val localRepository: LocalRepository
) {

    private val TAG = RecipeRepository::class.java.simpleName

    fun observeRecipes(
        scope: CoroutineScope,
        callback: (Result<List<Recipe>>) -> Unit
    ) {
        scope.launch(Dispatchers.IO + getJobErrorHandler()) {
            callback(Result.loading())
            val response = recipeDataSource.fetchRecipes()
            when (response.status) {
                Result.Status.SUCCESS -> {
                    response.data?.let {
                        Log.d(TAG, "observeRecipes: ${it.size}")
                        localRepository.saveRecipes(it)
                    }

                    callback(Result.success("", localRepository.getRecipeList()))
                }
                Result.Status.ERROR -> {
                    when {
                        localRepository.getRecipeListSize() > 0 -> callback(
                            Result.success("", localRepository.getRecipeList())
                        )
                        else -> {
                            response.message?.let {
                                callback(Result.error("", it))
                            }
                        }
                    }
                }
                Result.Status.LOADING -> TODO()
            }
        }
    }

    private fun getJobErrorHandler() = CoroutineExceptionHandler { _, e ->
        postError(e.message ?: e.toString())
    }

    private fun postError(message: String) {
        Log.e(TAG, "An error happened: $message")
    }
}
