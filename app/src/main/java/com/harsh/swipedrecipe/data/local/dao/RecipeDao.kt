package com.harsh.swipedrecipe.data.local.dao

import androidx.room.*
import com.harsh.swipedrecipe.data.local.model.Recipe

@Dao
interface RecipeDao {
    /**
     * Get all Recipes
     */
    @Transaction
    @Query("SELECT * FROM recipe ORDER BY id")
    fun getAllRecipes(): List<Recipe>

    /**
     * Get Recipe by id
     */
    @Transaction
    @Query("SELECT * FROM recipe WHERE id =:id")
    fun getRecipeById(id: Long): Recipe

    /**
     *Get Recipes Size
     */
    @Transaction
    @Query("SELECT Count(*) FROM recipe")
    fun getRecipesSize(): Int

    /**
     * Insert all Recipes
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllRecipes(recipeList: List<Recipe>)

    /**
     * Delete all Recipes
     */
    @Query("DELETE FROM recipe")
    fun deleteAllRecipes()
}