package com.harsh.swipedrecipe.data.remote.datasource

import com.harsh.swipedrecipe.data.remote.BaseDataSource
import com.harsh.swipedrecipe.data.remote.RecipeService
import javax.inject.Inject

class RecipeDataSource @Inject constructor(private val recipeService: RecipeService) :
    BaseDataSource() {
    suspend fun fetchRecipes() = getResult { recipeService.fetchRecipes() }
}
