package com.harsh.swipedrecipe.data.local

import com.harsh.swipedrecipe.data.local.dao.RecipeDao
import com.harsh.swipedrecipe.data.local.model.Recipe
import com.harsh.swipedrecipe.data.remote.dto.RemoteRecipe
import javax.inject.Inject

class LocalRepository @Inject
constructor(private val dao: RecipeDao) {

    /**
     * Insert all remote recipe into database
     */
    fun saveRecipes(remoteRecipeModel: List<RemoteRecipe>) {
        clearDatabase()
        val recipeList: ArrayList<Recipe> = arrayListOf()
        for (remoteRecipeItem in remoteRecipeModel) {
            val recipe = Recipe()

            remoteRecipeItem.id?.let {
                recipe.id = it
            }
            remoteRecipeItem.name?.let {
                recipe.name = it
            }
            remoteRecipeItem.image?.let {
                recipe.image = it
            }
            remoteRecipeItem.category?.let {
                recipe.category = it
            }
            remoteRecipeItem.label?.let {
                recipe.label = it
            }
            remoteRecipeItem.price?.let {
                recipe.price = it
            }
            remoteRecipeItem.description?.let {
                recipe.description = it
            }
            recipeList.add(recipe)
        }
        dao.insertAllRecipes(recipeList)
    }

    /**
     * Get All Recipe
     */
    fun getRecipeList() = dao.getAllRecipes()

    /**
     * Get Recipe List Size
     */
    fun getRecipeListSize() = dao.getRecipesSize()

    /**
     * Clear all Recipes
     */
    private fun clearDatabase() {
        dao.deleteAllRecipes()
    }

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: LocalRepository? = null

        fun getInstance(dao: RecipeDao) =
            instance ?: synchronized(this) {
                instance
                    ?: LocalRepository(dao).also { instance = it }
            }
    }
}