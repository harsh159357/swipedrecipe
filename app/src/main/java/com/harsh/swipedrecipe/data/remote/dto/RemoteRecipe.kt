package com.harsh.swipedrecipe.data.remote.dto

import com.google.gson.annotations.SerializedName

/**
 * Fields for Individual Recipe coming from Json
 */
data class RemoteRecipe(

    @SerializedName("id")
    var id: Long? = 0,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("image")
    var image: String? = null,

    @SerializedName("category")
    var category: String? = null,

    @SerializedName("label")
    var label: String? = null,

    @SerializedName("price")
    var price: Double? = 0.0,

    @SerializedName("description")
    var description: String? = null
)