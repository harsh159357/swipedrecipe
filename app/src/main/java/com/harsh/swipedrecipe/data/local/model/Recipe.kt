package com.harsh.swipedrecipe.data.local.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "recipe")
data class Recipe(
    @PrimaryKey
    var id: Long,

    var name: String,

    var image: String,

    var category: String,

    var label: String,

    var price: Double,

    var description: String

) : Parcelable {
    constructor() : this(
        0, "", "", "", "", 0.0, ""
    )
}