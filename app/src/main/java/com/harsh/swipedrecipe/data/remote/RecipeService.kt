package com.harsh.swipedrecipe.data.remote

import com.harsh.swipedrecipe.data.remote.dto.RemoteRecipe
import retrofit2.Response
import retrofit2.http.GET

interface RecipeService {
    @GET("he-public-data/reciped9d7b8c.json")
    suspend fun fetchRecipes(): Response<List<RemoteRecipe>>
}